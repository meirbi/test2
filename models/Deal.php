<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "deal".
 *
 * @property integer $id
 * @property integer $leadid
 * @property string $name
 * @property integer $amount
 *
 * @property Lead $lead
 */
class Deal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'deal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'leadid', 'name', 'amount'], 'required'],
            [['id', 'leadid', 'amount'], 'integer'],
            [['name'], 'string', 'max' => 55],
            [['leadid'], 'exist', 'skipOnError' => true, 'targetClass' => Lead::className(), 'targetAttribute' => ['leadid' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'leadid' => 'Leadid',
            'name' => 'Name',
            'amount' => 'Amount',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLead()
    {
        return $this->hasOne(Lead::className(), ['id' => 'leadid']);
    }
	
	public function getDealItem() ///////////////////////// 4b
    {
		///Add a comment to this line
        return $this->hasOne(Lead::className(), ['id' => 'leadid']);
    }

}
