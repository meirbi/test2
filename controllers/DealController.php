<?php

namespace app\controllers;

use Yii;
use app\models\Deal;
use app\models\DealSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UnauthorizedHttpException;///////////////

/**
 * DealController implements the CRUD actions for Deal model.
 */
class DealController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Deal models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DealSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Deal model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Deal model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
	 
	 
    /* public function actionCreate()  /////////////// את זה מחקתי
    {
        $model = new Deal();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
		*/
		
	public function actionCreate() ///////////////////////// 3b את זה הכנסתי במקום
    {
        
		if (!\Yii::$app->user->can('createDeal'))
			throw new UnauthorizedHttpException ('Hey, You are not allowed to create new deals');		
		
		$model = new Deal();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }	
		
	
    /**
     * Updates an existing Deal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
	 
    /* public function actionUpdate($id)  /////////// מחקתי את זה
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
	*/
	
	public function actionUpdate($id) /////////////////////////  3b והכנסתי את זה 
    {
        $model = $this->findModel($id);

		if (!\Yii::$app->user->can('updateDeal') && 
		    !\Yii::$app->user->can('updateOwnDeal', ['deal' =>$model]) )
			throw new UnauthorizedHttpException ('Hey, You are not allowed to update deals');		
			$load = $model->load(Yii::$app->request->post());
			$owner = $model->getDirtyAttributes();
			if(isset(Yii::$app->request->post()['Deal']['owner']) && !\Yii::$app->user->can('updateDeal'))
				throw new UnauthorizedHttpException ('Hey, You are not allowed to update deal owner');
        if ($load && $model->save()) {
			return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
	
    /**
     * Deletes an existing Deal model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
     public function actionDelete($id) ///////////////// 3b
    {
        
		if (!\Yii::$app->user->can('createDeal')) 
			throw new UnauthorizedHttpException ('Hey, You are not allowed to delete deals');
		$this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Deal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Deal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Deal::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
