<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\web\UrlManager\Url;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
			return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->homeUrl = ['site/index'];	
		Yii::$app->user->logout();

		return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function actionAbout()
    {
        return $this->render('about');
    }
	////////////////////////////////////////////
		public function actionTlpermissions()
	{
		$auth = Yii::$app->authManager;
		
		$createDeal = $auth->createPermission('createDeal');
		$createDeal->description = 'Admin can create new deals';
		$auth->add($createDeal);
		
		$updateDeal = $auth->createPermission('updateDeal');
		$updateDeal->description = 'Team leader can update
									dead including assignment';
		$auth->add($updateDeal);		
		
		$deleteDeal = $auth->createPermission('deleteDeal');
		$deleteDeal->description = 'Team leader can delete
									dead including assignment';
		$auth->add($deleteDeal);	
	}
	
		public function actionChilds()
	{
		$auth = Yii::$app->authManager;				
		$teamleader = $auth->getRole('teamleader');
	
		
		$updateDeal = $auth->getPermission('updateDeal');
		$auth->addChild($teamleader, $updateDeal);
		
		$createDeal = $auth->getPermission('createDeal');
		$auth->addChild($teamleader, $createDeal);

		$deleteDeal = $auth->getPermission('deleteDeal');
		$auth->addChild($teamleader, $deleteDeal);

	}
	///////////////////////////////////////////////////////////
	
}
